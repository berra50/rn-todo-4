import React, { FC } from "react";
import { StyleSheet, Text, View } from "react-native";

type TextProps = {
  text: string;

}

const Input: FC<TextProps> = ({text}) => {
  return (
    <View style={styles.item}>
      <View style={styles.itemLeft}>
        <View style={styles.circle}></View>
        <Text style={styles.itemText}>{text.ident}</Text>
      </View>
      <View style={styles.buttons}>
      <View >
        <Text style={styles.circular}>🗑️</Text>
      </View>
      <View >
        <Text style={styles.circular}>✍️</Text>
      </View>
    </View>
    </View>
  );
};


export default Input;

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#FFF',
        padding: 15,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    circle:{
      width: 24,
      height: 24,
      backgroundColor: 'purple',
      borderRadius: 50,
      opacity: 0.4,
      marginRight: 15,
  },
    itemText: {
      maxWidth: '80%',
    },
    circular: {
      fontSize: 20,
      marginLeft: 5,
    },
    buttons: {
      flexDirection: "row",
    }
});