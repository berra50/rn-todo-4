import React, { FC, useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  Keyboard,
} from "react-native";
import Item from "../components/Item";
import { db } from "../firebase";

const HomeScreen: FC = () => {
  const [task, setTask] = useState<string>("");
  const [taskItems, setTaskItems] = useState<string[]>([]);
  const [taskItemsObz, setTaskItemsObz] = useState<string[]>([]);

  useEffect(() => {
    getCards().then((result) => {
        setTaskItems(result);
    });
  }, [taskItemsObz]);



  const handleAddTask = () => {
    Keyboard.dismiss();
    let newTask: object = { ident: task}
    addCard(newTask);
  };



  const completeTask = (deleteItem: object) => {
    deleteCard(deleteItem.id);

  };



  async function getCards() {
    const snapshot = await db.collection("CardTask").get();

    const documents = [];
    documents.push(snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    return documents[0];
  }




  function addCard(newTask:object) {
    db.collection("CardTask")
      .add(newTask)
      .then(() => {});

      setTaskItemsObz([...taskItemsObz,"added"]);
  }



  function deleteCard(deleteItem:string) {  
    var jobskill_query = db
      .collection("CardTask")
      .doc(deleteItem);
      jobskill_query.delete();
    
      setTaskItemsObz([...taskItemsObz,"deleted"]);

    };


 
  return (
    <View style={styles.container}>
     <View style= {styles.taskSection}>
     <Text style={styles.Title}>Today's Task📜</Text>


      <KeyboardAvoidingView 
      behavior="padding" 
      style={styles.writeTaskWrapper}>
        <TextInput
          style={styles.input}
          placeholder={"Write a task"}
          value={task}
          onChangeText={(text) => {
            setTask(text);
          }}
        />
        
        <TouchableOpacity onPress={handleAddTask}>
          <View style={styles.addWrapper}>
            <Text style={styles.addText}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
        keyboardShouldPersistTaps="handled"
      >
        <View style={styles.tasksWrapper}>
          <View style={styles.items}>
            {taskItems?.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    completeTask(item);
                  }}
                >
                  <Item text={item} />
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
        </ScrollView>
        </View>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E8EAED",
  },
  tasksWrapper: {
    paddingHorizontal: 10,
  },
  taskSection:{
    paddingTop: 20,
    paddingHorizontal: 20,
  },

  items: {
    marginTop: 15,
  },
  Title:{
    fontSize: 23,
    fontWeight: 'bold',
    paddingLeft: 8,
    paddingBottom: 10,
  },
  writeTaskWrapper: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: 5,
  },
  input: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "#FFF",
    borderRadius: 40,
    borderColor: "#C0C0C0",
    borderWidth: 1,
    width: 300,
  },
  addWrapper: {
    width: 55,
    height: 55,
    backgroundColor: "#FFF",
    borderRadius: 55,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C0C0C0",
    borderWidth: 1,
  },
  addText: {},
});
